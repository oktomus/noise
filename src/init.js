window.onload = function(){
	var 	canvas 		= document.getElementById("board"),
	ctx 		= canvas.getContext("2d"),
	cw		= canvas.width	= window.innerWidth,
	ch		= canvas.height = window.innerHeight;

	function setPixel(imageData, x, y, r, g, b, a){
		index = (x + y * imageData.width) * 4;
		imageData.data[index+0] = r;
		imageData.data[index+1] = g;
		imageData.data[index+2] = b;
		imageData.data[index+3] = a;
	}


	ctx.clearRect(0, 0, cw, ch);
	var t0 = new Date().getTime();
	imageData = ctx.createImageData(cw, ch);
	for (var i = 0; i < 100000; ++i) {
		var x = Math.floor(Math.random() * cw);
		var y = Math.floor(Math.random() * ch);
		var r = Math.floor(Math.random() * 256);
		var g = Math.floor(Math.random() * 256);
		var b = Math.floor(Math.random() * 256);
		setPixel(imageData, x, y, r, g, b, 255);
	}
	
	ctx.putImageData(imageData, 0, 0);
	var t1 = new Date().getTime();
	console.log('setPixel() method: ' + (t1 - t0));

};
